#include "exampleplayer.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {
    /* 
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */

  othelloboard = new Board();
  playerc = side;
  if (playerc == BLACK)
    {
      opponentc = WHITE;
    }
  else
    {
      opponentc = BLACK;
    }


}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
}


/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    /* 
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's  move before calculating your own move
     */ 
  Move * temp = NULL;
  Board * boardcopy;
  othelloboard->doMove(opponentsMove, opponentc);
  Move * newmove;
  int i, j, score;
  int bestscore = -10000;

  if (othelloboard->hasMoves(playerc) == false)
    {
      /* No more moves */
      return NULL;
    }
  else
    {
      for (i=0; i<8; i++)
	{
	  for (j=0; j<8; j++)
	    {
	      /* i and j are the x, y coordinates. Must be between 0 and 8. */
	      newmove = new Move(i, j);
	      if (othelloboard->checkMove(newmove, playerc) == true)
		{
		  /* Check if the move is legal. If it is, make the move. */
		  boardcopy = othelloboard->copy();
		  boardcopy->doMove(newmove, playerc);
		  score = boardcopy->countBlack() - boardcopy->countWhite();
		  int x = newmove->getX();
		  int y = newmove->getY();

		  if (x == 0 || x == 7 || y == 0 || y == 7)
		    {/*edges */
		      score = score + 4;
		    }
		  if ((x == 0 && y == 0) || (x == 0 && y == 7) || (x == 7 && y == 0) || (x == 7 && y == 7))
		    {/*corners*/
		      score = score + 8;
		    }
		  if ((x == 0 && y == 1) || (x == 1 && y == 0) || (x == 0 && y == 6) || (x == 1 && y == 7) || 
		      (x == 6 && y == 0) || (x == 7 && y == 1) || (x == 6 && y == 7) || (x == 7 && y == 6))
		    {/*adjacent to corner (bad)*/
		      score = score - 8;
		    }
		  if ((x == 1 && y == 6) || (x == 1 && y == 1) || (x == 6 && y == 1) || (x == 6 && y == 6))
		    {
		      score = score - 5;
		    }

		  if (x == 1 || x == 6 || y == 1 || y == 6)
		    {/*adjacent to edge (bad) */
		      score = score - 4;
		    }
		  
		  if (score > bestscore)
		    {
		      bestscore = score;
		      temp = new Move(newmove->getX(), newmove->getY());
		      
		    }
		}
	    }
	}
    }
  delete boardcopy;
  delete newmove;
  othelloboard->doMove(temp, playerc);
  return temp;
}  
